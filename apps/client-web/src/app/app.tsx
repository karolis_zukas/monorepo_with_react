import React, { Suspense, lazy } from 'react';
import { Route } from 'react-router-dom';
import './app.scss';
import SideMenu from './side-menu/side-menu';
import Header from './header/header';
import { Page, LoadingAnimated } from '@stock-exchange/ui-library';

// TODO: this is bad, but currently there is issues while importing from module
const Landing = lazy(() => import('../../../../libs/landing/src/lib/landing'));
const RequestMedia = lazy(() => import('../../../../libs/request-media/src/lib/request-media'));
const FindWork = lazy(() => import('../../../../libs/find-work/src/lib/find-work'));
const About = lazy(() => import('../../../../libs/about/src/lib/about'));
const Login = lazy(() => import('../../../../libs/login/src/lib/login'));
const Register = lazy(() => import('../../../../libs/register/src/lib/register'));

export const App = () => {
  return (
    <div className="app">
      <Page 
        header={<Header />}
        sidebar={<SideMenu />}
        content={
          <>
            <Suspense fallback={<LoadingAnimated />}>
              <Route path="/" exact component={Landing} />
              <Route path="/about" component={About} />
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />
              <Route path="/request" component={RequestMedia} />
              <Route path="/find-work" component={FindWork} />
            </Suspense>
          </>
        }
      footer={<div><h2>Footer</h2></div>}
      />
    </div>
  );
};

export default App;
