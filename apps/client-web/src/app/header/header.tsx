import React from 'react';
import { UserPlus, User, HelpCircle } from 'react-feather';
import { Link } from 'react-router-dom';
import ErrorBoundary from '../error-boundary/ErrorBoundary';

import './header.scss';
import { Button } from '@stock-exchange/ui-library';

/* eslint-disable-next-line */
export interface HeaderProps {}

export const Header = (props: HeaderProps) => {

  return (
    <header className="header">
      <div className="header__title-container">
        <h1 className="header__title">Stock Exchange</h1>
        <h2 className="header__subtitle">smart subtitle</h2>
      </div>

      <ErrorBoundary>
        <div className="header__button-container">
          <Button><Link to="/login"><User size={20} />Login</Link></Button>
          <Button><Link to="/register"><UserPlus size={20} />Register</Link></Button>
          <Button><Link to="/about"><HelpCircle size={20} />About</Link></Button>
        </div>
      </ErrorBoundary>
    </header>
  );
};

export default Header;
