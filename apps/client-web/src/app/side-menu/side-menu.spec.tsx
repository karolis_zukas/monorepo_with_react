import React from 'react';
import { render } from '@testing-library/react';
import SideMenu from './side-menu';

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));

jest.mock('@stock-exchange/ui-library', () => ({
  Logo: () => <div>Logo</div>,
  ButtonIcon: () =>  <div>Icon</div>,
  ButtonIconFlat: () => <div>IconFlat</div>,
  Button: () => <button>Button</button>
}));

describe(' SideMenu', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SideMenu />);
    expect(baseElement).toBeTruthy();
  });
});
