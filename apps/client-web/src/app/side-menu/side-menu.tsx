import React from 'react';
import { useHistory, Link } from 'react-router-dom';
import {
  Logo,
  ButtonIcon,
  ButtonIconFlat,
  Button
} from '@stock-exchange/ui-library';
import {
  Home,
  Map,
  Camera,
  Phone,
  Book,
  User,
  UserPlus,
  ChevronLeft
} from 'react-feather';

import './side-menu.scss';
import ErrorBoundary from '../error-boundary/ErrorBoundary';

export const SideMenu = () => {
  const size = 18;
  const history = useHistory();

  const changeMenuState = () => {
    console.log('closing/opening the menu');
  };

  return (
    <ErrorBoundary>
      <div className="sidemenu-container">
        <Logo />
        <div className="sidemenu-container__collapse-button">
          <ButtonIconFlat circle
            onClick={() => changeMenuState()}
            icon={ <ChevronLeft /> } />
        </div>
        <p className="sidemenu-container__paragraph">A small paragraph of text in the side menu looks very cool and hipster.</p>

        <div className="menu-list-container">
          <ul className="menu-list menu-list--main">
            <li className="menu-list__item">
              <Button dark><Link to="/"><Home {...{size}} />Home</Link></Button>
            </li>
            <li className="menu-list__item">
              <Button dark><Link to="/request"><Map {...{size}} />Request</Link></Button>
            </li>
            <li className="menu-list__item">
              <Button dark><Link to="/find-work"><Camera {...{size}} />Work</Link></Button>
            </li>
          </ul>

          <ul className="menu-list menu-list--secondary">
            <li className="menu-list__item">
              <Button dark><Link to="/about"><Book {...{size}} />About</Link></Button>
            </li>
            <li className="menu-list__item">
              <Button dark><Link to="/contact"><Phone {...{size}} />TODO: Contact</Link></Button>
            </li>
          </ul>

          <div className="menu-list menu-list--footer">
            <ButtonIcon dark
              onClick={() => history.push('/login')}
              icon={ <User /> } />
            <ButtonIcon dark
              onClick={() => history.push('/register')}
              icon={ <UserPlus /> } />
          </div>
        </div>
      </div>
    </ErrorBoundary>
  );
};

export default SideMenu;
