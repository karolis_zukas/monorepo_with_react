import React from 'react';

import './about.scss';

/* eslint-disable-next-line */
export interface AboutProps {}

export const About = (props: AboutProps) => {
  return (
    <div>
      <h1>Welcome to about component!</h1>
    </div>
  );
};

export default About;
