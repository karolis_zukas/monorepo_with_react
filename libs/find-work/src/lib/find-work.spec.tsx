import React from 'react';
import { render } from '@testing-library/react';

import FindWork from './find-work';

describe(' FindWork', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FindWork />);
    expect(baseElement).toBeTruthy();
  });
});
