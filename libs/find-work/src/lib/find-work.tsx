import React from 'react';

import './find-work.scss';

/* eslint-disable-next-line */
export interface FindWorkProps {}

export const FindWork = (props: FindWorkProps) => {
  return (
    <div>
      <h1>Welcome to find-work component!</h1>
    </div>
  );
};

export default FindWork;
