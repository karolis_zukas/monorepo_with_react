import React from 'react';

import './landing.scss';

/* eslint-disable-next-line */
export interface LandingProps {}

export const Landing = (props: LandingProps) => {
  return (
    <div>
      <h1>Welcome to landing component!</h1>
    </div>
  );
};

export default Landing;
