import React from 'react';
import { render } from '@testing-library/react';

import RequestMedia from './request-media';

describe(' RequestMedia', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<RequestMedia />);
    expect(baseElement).toBeTruthy();
  });
});
