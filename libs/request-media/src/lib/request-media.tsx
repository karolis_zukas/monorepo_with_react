import React from 'react';

import './request-media.scss';

/* eslint-disable-next-line */
export interface RequestMediaProps {}

export const RequestMedia = (props: RequestMediaProps) => {
  return (
    <div>
      <h1>Welcome to request-media component!</h1>
    </div>
  );
};

export default RequestMedia;
