import React, { ReactNode } from 'react';
import './button-icon.scss';

export interface ButtonIconProps {
  dark?: boolean;
  circle?: boolean;
  icon: ReactNode;
  onClick: Function;
}

export const ButtonIcon = ({ onClick, icon, dark, circle }: ButtonIconProps) => {
  return (
    <div className={`
      button-icon-container
      button-icon-container--${dark ? 'dark': 'light'}
      button-icon-container--${circle ? 'circle': 'squere'}
    `} onClick={ ()=> onClick() }>
      <span>{ icon }</span>
    </div>
  );
};

export const ButtonIconFlat = ({ onClick, icon, dark, circle }: ButtonIconProps) => {
  return (
    <div className={`
      button-icon-flat-container
      button-icon-flat-container--${dark ? 'dark': 'light'}
      button-icon-flat-container--${circle ? 'circle': 'squere'}
    `} onClick={ ()=> onClick() }>
      <span>{ icon }</span>
    </div>
  );
};

export default ButtonIcon;
