import React, { ReactNode } from 'react';
import './button.scss';

export interface ButtonProps {
  onClick?: Function;
  children: string | ReactNode;
  dark?: boolean;
}

export const Button = ({ onClick, children, dark }: ButtonProps) => {
  return (
    <div className={`button-container button-container--${dark ? 'dark': 'light'}`} onClick={ ()=> onClick ? onClick() : null }>
      <span>{ children }</span>
    </div>
  );
};

export default Button;
