import React, { ReactNode } from 'react';

import './card.scss';

/* eslint-disable-next-line */
export interface CardProps {
  title?: string;
  subtitle?: string;
  children: ReactNode;
  footerContent?: ReactNode;
}

export const Card = ({ title, subtitle, children, footerContent }: CardProps) => {
  return (
    <div className="card">
      <div className="card-header">
        <h3 className="card-header__title">{ title }</h3>
        <h4 className="card-header__subtitle">{ subtitle }</h4>
      </div>
      <div className="card-content">
        { children }
      </div>
      { footerContent && <div className="card-footer">{ footerContent }</div> }
    </div>
  );
};

export default Card;
