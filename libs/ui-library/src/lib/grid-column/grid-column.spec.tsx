import React from 'react';
import { render } from '@testing-library/react';

import GridColumn from './grid-column';

describe(' GridColumn', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<GridColumn />);
    expect(baseElement).toBeTruthy();
  });
});
