import React, { ReactNode } from 'react';
import './grid-column.scss';

export interface GridColumnProps {
  children: ReactNode;
  layout: string;
}

export const GridColumn = ({ children, layout }: GridColumnProps) => {
  return (
    <div className={ layout } >
      { children }
    </div>
  );
};

export default GridColumn;
