import React, { ReactNode } from 'react';
import './grid-row.scss';

export interface GridRowProps {
  children: ReactNode[] | ReactNode;
}

export const GridRow = ({ children }: GridRowProps) => {
  return (
    <div className="row">
      { children }
    </div>
  );
};

export default GridRow;
