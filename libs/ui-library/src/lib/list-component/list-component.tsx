import React from 'react';

import './list-component.scss';

/* eslint-disable-next-line */
export interface ListComponentProps {}

export const ListComponent = (props: ListComponentProps) => {
  return (
    <div>
      <h1>Welcome to list-component component!</h1>
    </div>
  );
};

export default ListComponent;
