import React from 'react';
import { render } from '@testing-library/react';

import LoadingAnimated from './loading-animated';

describe(' LoadingAnimated', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LoadingAnimated />);
    expect(baseElement).toBeTruthy();
  });
});
