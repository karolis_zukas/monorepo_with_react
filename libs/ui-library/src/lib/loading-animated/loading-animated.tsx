import React from 'react';

import './loading-animated.scss';

/* eslint-disable-next-line */
export interface LoadingAnimatedProps {}

export const LoadingAnimated = (props: LoadingAnimatedProps) => {
  return (
    <div>
      <h1>Welcome to loadingAnimated component!</h1>
    </div>
  );
};

export default LoadingAnimated;
