import React from 'react';

import './logo.scss';

export interface LogoProps {
  dark?: boolean;
}

// TODO: style in dark and light colors to use on black and white backgrounds
export const Logo = (props: LogoProps) => {
  return (
    <div className="logo-container">
      <h3 className="logo-title">Stock</h3>
      <h4 className="logo-subtitle">exchange</h4>
    </div>
  );
};

export default Logo;
