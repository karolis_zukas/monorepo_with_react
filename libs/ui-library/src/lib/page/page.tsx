import React, { ReactNode } from 'react';

import './page.scss';

/* eslint-disable-next-line */
export interface PageProps {
  sidebar?: ReactNode;
  content: ReactNode;
  header?: ReactNode;
  footer?: ReactNode;
}

export const Page = ({ header, footer, content, sidebar }: PageProps) => {
  return (
    <div className="page-container">
        <div className="page-container__sidebar">
          { sidebar }
        </div>

      <div className="page-content">
        <div className="page-content__header">
          { header }
        </div>

        <div className="page-content__content">
          { content }
        </div>

        <div className="page-content__footer">
          { footer }
        </div>
      </div>
      
    </div>
  );
};

export default Page;
