import React from 'react';

import './ui-library.scss';

/* eslint-disable-next-line */
export interface UiLibraryProps {}

export const UiLibrary = (props: UiLibraryProps) => {
  return (
    <div>
      <h1>Welcome to ui-library component!</h1>
    </div>
  );
};

export default UiLibrary;
